# Changelog

## Version 1.5

- Fixed a clash between the packages `newtx` and `amssymb`.

- Updated licesing information and added the LPPL license file.

- Created README with usage instructions to project root.

- Renamed changelog

- Added document metadata for the purpose of creating valid PDF/A documents.

- Fixed minor typos

## Version 1.4

- Minor terminology fixes

## Version 1.3

- Some updates to appearance.

- Removed front matter entries from ToC.

- Fixed a problem with default document class options.

## Version 1.2

- Updated the template to conform to new appearance guidelines.

- Increased the width of the glossary for it to take less space.

- Added compilation instructions to main.tex as well.

- Fixed encoding issues relating to listings fix for Scandinavian letters in
  code comments.

## Version 1.1

- Replaced the glossaries package option xindy to automake. Now the template
  does not require Perl to be installed.

- Added instructions how to compile the thesis using this template into the
  conclusion text.

## Version 1.0

- First published template.
