# Tampere University Bachelor's Thesis Template

This repository contains the Bachelor's thesis template of Tampere University
in LaTeX-form.

## Local editing and compilation

To fully compile the project locally, add a LaTeΧ-installation such as [TeX
Live] or [MacTeX] to your system and run

	texprogram main.tex &&
	biber main &&
	makeindex main.tex &&
	texprogram main.tex &&
	texprogram main.tex

where `texprogram` ∈ {`pdflatex`, `lualatex`} in this directory. This should
result in a PDF/A file being created in this folder.

The project may be edited with any text editor, but some are better suited to
the purpose than others. Some alternatives are

- [VS Codium] or [VS Code] with the [LaTeX Workshop] extension and
- [Vim] with the [vim-latex] or [vimtex] addons.

Simply edit the `.tex` files in the repository as needed and compile the
`main.tex` file as instructed above to see the results.

<!-- Links -->

[TeX Live]: https://www.tug.org/texlive/
[MacTeX]: https://www.tug.org/mactex/
[VS Codium]: https://vscodium.com/
[VS Code]: https://code.visualstudio.com/
[LaTeX Workshop]: https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop
[Vim]: https://www.vim.org/
[vim-latex]: https://github.com/vim-latex/vim-latex
[vimtex]: https://github.com/lervag/vimtex

## Using Overleaf

If you do not wish to install LaTeX locally, an alternative way of writing and
compiling LaTeX documents (and this project) is on the online LaTeΧ service
[Overleaf]. You simply need to

1. download a zipped version of this project by clicking <kbd>Download</kbd> →
   <kbd>zip</kbd> on this page,

2. create a personal account on the Overleaf service via their [registration
   page][Overleaf registration] or [log in][Overleaf login] and

3. click on <kbd>New Project</kbd> → <kbd>Upload project</kbd> on your
   Overleaf home page and select the zipped file you downloaded in item 1.

This will result in a new Overleaf project being created based on the uploaded
zip-file.

You can then use the Overleaf online editor to write your thesis. Final or
intermediary versions of the thesis can be downloaded by pressing
<kbd>Download PDF</kbd> on the project page on Overleaf.

<!-- Links -->

[Overleaf]: https://www.overleaf.com/
[Overleaf registration]: https://www.overleaf.com/register
[Overleaf login]: https://www.overleaf.com/login

## Note on document information

You should fill in your thesis information in the variable definitions at the
start of `main.tex`. They are of the form

	\def\variable{<value>} ,

where `<value>` should be filled in, unless it already is what you need it to
be. These will be used in generating the metadata for the thesis, needed in
archiving it in [Trepo], the Tampere University publication repository. The
variable names should be descriptive enough for you to figure out what the
braces need to contain.

[Trepo]: https://trepo.tuni.fi/

## License

This work may be distributed and/or modified under the conditions of the LaTeX
Project Public License, either version 1.3 of this license or (at your option)
any later version. The latest version of this license is in
<http://www.latex-project.org/lppl.txt> and version 1.3 or later is part of
all distributions of LaTeX version 2005/12/01 or later.

This work has the LPPL maintenance status `maintained'. The Current Maintainer
of this work is Santtu Söderholm (<santtu.soderholm@tuni.fi>).

This work consists of the files `tauthesis.cls`.
